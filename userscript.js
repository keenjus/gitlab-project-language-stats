// ==UserScript==
// @name         GitLab Languages Statistics
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description
// @author       keenjus
// @match        https://gitlab.com/*/*$
// @grant        none
// @require      https://gitlab.com/keenjus/cache-manager/raw/master/index.js
// ==/UserScript==

(function () {
    'use strict';
    const $projectId = document.querySelector("#project_id");
    if(!$projectId) return;

    const projectId = $projectId.value;
    if (!projectId) return;

    const cacheManager = new CacheManager();
    cacheManager.getOrCreate("lstats_"+projectId, 1800 * 1000, () => {
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.addEventListener("load", function () {
                const languages = JSON.parse(this.responseText);
                resolve(languages);
            });
            request.open("GET", `https://gitlab.com/api/v4/projects/${projectId}/languages`);
            request.send();
        });
    }).then(data => {
        createLanguageStatisticsElement(data);
    });

    function createLanguageStatisticsElement(languages) {
        // doesn't quite work like intended yet
        const referenceElement = document.querySelector(".limit-container-width > #content-body .project-stats");

        const wrapperElement = document.createElement("div");
        wrapperElement.className = "container-fluid container-limited";
        wrapperElement.style.maxWidth = "990px";
        wrapperElement.style.paddingBottom = "5px";

        const stackedBarElement = document.createElement("div");
        stackedBarElement.style.height = "9px";
        stackedBarElement.style.display = "table";
        stackedBarElement.style.width = "100%";
        stackedBarElement.style.overflow = "hidden";
        stackedBarElement.style.whiteSpace = "nowrap";
        stackedBarElement.style.border = "1px solid #ddd";
        stackedBarElement.style.borderTop = "0";
        stackedBarElement.style.borderBottomRightRadius = "3px";
        stackedBarElement.style.borderBottomLeftRadius = "3px";

        const keys = [];
        const values = [];

        for(const key in languages){
            keys.push(key);
            values.push(languages[key]);
        }

        if (values.length > 1) {
            const lastValueIndex = values.length - 1;
            const lastValue = 100 - values.slice().splice(0, lastValueIndex).reduce((sum, n) => sum + n);

            values[lastValueIndex] = Math.round(lastValue * 100) / 100;
        }

        for (const i in values) {
            const percentage = values[i];

            const languageElement = document.createElement("span");
            languageElement.style.width = percentage + "%";
            languageElement.style.backgroundColor = getColor(keys[i]);
            languageElement.style.display = "table-cell";
            languageElement.style.lineHeight = "8px";
            languageElement.style.textIndent = "-9999px";

            languageElement.dataset.placement = "bottom";
            languageElement.dataset.toggle = "tooltip";
            languageElement.dataset.container = "body";
            languageElement.dataset.title = keys[i];

            stackedBarElement.appendChild(languageElement);
        }

        wrapperElement.appendChild(stackedBarElement);

        insertAfter(wrapperElement, referenceElement);
    }

    function getColor(languageName) {
        switch (languageName) {
            case "TypeScript":
                return "#2b7489";
            case "JavaScript":
                return "#f1e05a";
            case "HTML":
                return "#e34c26";
            case "C#":
                return "#178600";
            case "CSS":
                return "#563d7c";
            case "Vue":
                return "#4fc08d";
            case "Kotlin":
                return "#f18e33";
            case "Java":
                return "#b07219";
            default:
                return randomColor();
        }
    }

    function randomColor() {
        const letters = "0123456789ABCDEF".split("");
        let color = "#";
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }
})();